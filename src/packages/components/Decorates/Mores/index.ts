import { WeatherConfig } from './Weather/index'
import { TimeConfig } from './Time/index'
import { WeatherTimeConfig } from './WeatherTime/index'
import { NumberConfig } from './Number/index'
import { TimeCommonConfig } from './TimeCommon/index'

export default [WeatherConfig, TimeCommonConfig, TimeConfig, WeatherTimeConfig, NumberConfig]
